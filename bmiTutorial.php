<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Body Mass Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">Body Mass Tutorial</h1>
			<h3>Where the data was sourced.</h3>
			<p>The data for BMI page was taken from the World Health Organisation.</p>
			
			<h3>How the data was obtained. </h3>
			<p>The data taken from the World Health Organisation is can be returned in an XML format but to be consitent with the rest of our data we formated it into json data by using "format=json". Then using AJAX and Jquery to obtain the data.</p>
			
			<h3>How the data was visualised.</h3>
			<p>To visualise this data we used a line chart. The library we used for this chart was chart.js. Chart.js is a simple and easy to use open source library that allows developers to use to display different types of charts using javascript. To allow the user to interact with this we create a drop down menu with a list of all the countries with data. Once the selection had been made information about male and female is returned in the line graph.</p>
		
			<h3>Learn more about Chart.JS.  </h3>
			<p>This is a link to the<a href ="https://www.chartjs.org/docs/latest/charts/line.html"> Chart.js</a> website. </p>
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>
