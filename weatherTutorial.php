<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Weather Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">Weather Tutorial</h1>
			<h3>Sourceing Weather Data</h3>
			<p>To create our weather page we collected the data used from <a href ="https://www.apixu.com/api.aspx">APIXU weather API.</a> APIXU provide data on real-time weather, Historical data and many more. </p>
			
			<h3>How to Collect and Display Weather Data</h3>
			<p> To display up to date data we use API's which allow to present dynamic data. To allow the user to choose what weather they would like to see using latlng for the location selected. By using the specific coordinates it allows for accurate data to be retrievd. Geocoding is used to retrieve the loaction and coverting it into the latitude and longitude of that area, it is a service provided by the <a href ="https://developers.google.com/maps/documentation/javascript/geocoding">google maps platfrom</a>. To recieve the data from the weather API we use AJAX. for more information about AJAX please check out our <a href ="javaScriptTutorial.php">JavaScript Tutorial.</a> </p>
			
			<h3>How to Visualise Weather Data </h3>
			<p>To dispaly the data we used google maps and google maps marker constructor to allow the user to drag the marker to the area they would like to see the weather for. The box will show an image of the current conditions in that area. </p>
			
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>