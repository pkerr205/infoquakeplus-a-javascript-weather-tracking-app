<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Life Expectancy Page</title>

		<?php include("partial/_meta"); ?>

		<?php include("partial/_scripts.php"); ?>

		<link rel="stylesheet" type="text/css" href="css/chartStyle.css">
		<script src = "https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script src ="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
	</head>

	<body>

		<div class="d-flex" id="wrapper">
		
		<?php include("partial/_sidebar"); ?>	
		

		<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
		
		
		 <h4>Average Life expectancy by Country 
		  <br><small>Use the drop down box below to select a country you wish to display data on, data is filtered by gender.</small></h1>
	

	<div align="center" id="content" >
      
 <select class="form-control" style="width: 200px" id = "myList">
           
             </select><br>
  <div id="chartSize">
  <canvas id="myChart" height="130" ></canvas>
  </div>

	<script>
	
	
  
  var country;
  var contents = [];
  <!--- "localdata/chubbychaser.json", local data is there incase something breaks -->
  $.getJSON( "https://scottlovesfatties.github.io/dataworldbank/soremote.json", function( data){
       contents = (data);
        console.log(contents);
    
    }).done(function(){ 
   var country_names = [];
      var j=1;
      var name = contents[0]["Country or Area"];
     var male = [contents[0]["Value"], contents[3]["Value"],contents[6]["Value"]];
     $("#myList").append("<option>"+contents[0]["Country or Area"]+"</option>");
     var female = [contents[1]["Value"], contents[4]["Value"],contents[7]["Value"]];
     var both = [contents[0]["Value"], contents[5]["Value"],contents[8]["Value"]];
     for(var i = 1; i < contents.length; i ++)
      {
        if(name != contents[i]["Country or Area"]){
           name = contents[i]["Country or Area"] ;
            $("#myList").append("<option>"+contents[i]["Country or Area"]+"</option>");
        }
       }
    displayChart(male, female,both);
       
});
    var graph;
$("#myList").change(function(){
   var i;
 
   for(i= 0 ; i < contents.length; i ++)
    if(contents[i]["Country or Area"] == $("#myList").val())
         break;
        
   var male = [contents[i]["Value"], contents[i+3]["Value"],contents[i+6]["Value"],0];
   var female = [contents[i+1]["Value"], contents[i+4]["Value"],contents[i+7]["Value"],0];
   var both=[contents[i+2]["Value"], contents[i+5]["Value"],contents[i+8]["Value"],0];
   var data = {
                    labels: ["2012", "2000", "1990"],
                    datasets: [ 
              { 
                        label: "Male Average age",
                backgroundColor: "rgba(100, 100, 255, .4)",
                        borderColor: "#c0e",
                        borderWidth: 1,
                        hoverBackgroundColor: "#59597F",         
                        data: male
              },{ 
                        label: "Female Average age",
                backgroundColor: "rgba(195, 10, 100, .4)",
                        borderColor: "#c44",
                        borderWidth: 1,
                        hoverBackgroundColor: "#b942f4",         
                        data: female
              },{ 
                        label: "Male and Female Average age",
                backgroundColor: "rgba(0, 255, 0, .4)",
                        borderColor: "#c44",
                        borderWidth: 1,
                        hoverBackgroundColor: "#11ee55",         
                        data: both
              }
              ]
            };
           graph.data = data;
           graph.update();
})
function displayChart(male, female,both){
   
       var data = {
                    labels: ["2012", "2000", "1990"],
                    datasets: [ 
              { 
                        label: "Male Average age",
                backgroundColor: "rgba(100, 100, 255, .4)",
                        borderColor: "#c0e",
                        borderWidth: 1,
                        hoverBackgroundColor: "#59597F",         
                        data: male
              },{ 
                        label: "Female Average age",
                backgroundColor: "rgba(195, 10, 100, .4)",
                        borderColor: "#c44",
                        borderWidth: 1,
                        hoverBackgroundColor: "#b942f4",         
                        data: female
              },{ 
                        label: "Male and Female Average age",
                backgroundColor: "rgba(0, 255, 0, .4)",
                        borderColor: "#cf4",
                        borderWidth: 1,
                        hoverBackgroundColor: "#11ee55",         
                        data: both
              }
              ]
            };

            var options = { 
                                defaultFontSize: 2,
                                elements: {
                                    rectangle: {
                                        borderWidth: 2,
                                        borderColor: 'rgb(0, 255, 0)',
                                        borderSkipped: 'bottom'
                                    }
                                },
                                responsive: true,
                                legend: {
                                    position: 'right',
                                },
                                title: {
                                    display: true,
                                    text: 'Averge Life Expectancy each year'
                                }
                            };

                    
            var ctx = document.getElementById("myChart").getContext("2d");
                 
            
              graph = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: options
        });
          
}

</script>
		
	</div>
	</div>
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
	
</body>

</html>