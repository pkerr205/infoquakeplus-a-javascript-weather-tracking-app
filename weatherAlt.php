<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Weather Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.css"/>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.js"></script>

  <script type="text/javascript" src="https://cdn.aerisjs.com/aeris.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

</head>

<body>


	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		<?php include("partial/_header.php"); ?>
		

		
		<style>
    #map-canvas {
      width: 800px;
      height: 600px;
    }
  </style>

		<div class="container-fluid">
		
		<h1>Temperature Forcast (Metric)</h1>

	
	<div id="map-canvas"></div>
  
  <script type="text/javascript">
    // Configure Aeris API keys
    aeris.config.set({
      apiId: 'WKkVoFhlzpHaqBaVcBLRS',
      apiSecret: 'rKoQOBgOePL4vQf9qB6j2r5TN4NrtBP9goIpJQm6'
    });
    
    // Create the map, where 'map-canvas' is the id of an HTML element.
    var map = new aeris.maps.Map('map-canvas', {
      zoom: 2,
      center: [23.564,21.0938],
      baseLayer: new aeris.maps.layers.AerisTile({
        tileType: 'flat-dk',
        zIndex: 1
      })
    });
    
    // Create 'water-depth' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'water-depth',
      zIndex: 3,
      map: map
    });

    // Create 'admin-cities' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'admin-cities-dk',
      zIndex: 10,
      map: map
    });

    // Create 'ftemperatures' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'ftemperatures',
      opacity: 0.4,
      zIndex: 12,
      map: map
    });

    // Create 'ftemperatures-max-text' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'ftemperatures-max-text-metric-dk-lg',
      zIndex: 14,
      map: map
    });

    // Create 'ftemperatures-min-text' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'ftemperatures-min-text-metric-dk',
      zIndex: 16,
      map: map
    });
  </script>
	
	</div>
	
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>