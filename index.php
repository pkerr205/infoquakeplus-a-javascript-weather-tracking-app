<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Home Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

	
	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">
        <h1 class="mt-4">Home Page</h1>
        <p>Website summary</p>

		<p>Infoquake+ is a website that visualise and display data and provide tutorials on the techniques we used to do so and every page that maps some data links to it's respective tutorials, and vice-versa every tutorial links to the pages that demonstrate it. Infoquake+ collects and visualises data from around the globe displaying the likes of earthquake and weather information, and we have used several data visualisation techniques to display the information.<br> Enjoy! </p>
		<p>Link to PDF of the project specification</p>
		
		<!--<a href="Integrated Project 3 Specification January 2019 V0.1.pdf" class ="btn btn-outline-primary" download>-->
		<!--<button type="button" class="btn btn-outline-primary" download="Integrated Project 3 Specification January 2019 V0.1.pdf">PDF PROJECT SPECIFICATION</button>-->


		<a href="IGPSpecification.pdf" download="IGPSpecification.pdf">


	<!--	<a href="IGPSpecification.pdf" download="Integrated Project 3 Specification January 2019 V0.1.pdf">
-->
		<button type="button" class="btn btn-outline-primary">DOWNLOAD PDF PROJECT SPECIFICATION</button>
		</a>
		
		<p>&nbsp;</p>
		
		<p>Link to PDF of the project initiation document</p>
		
		<a href="IP3_Project_Initiation_Group_20.pdf" download="IP3_Project_Initiation_Group_20.pdf">
		<button type="button" class="btn btn-outline-primary">DOWNLOAD PDF PROJECT INITIATION DOCUMENT</button>
		</a>
		
		
		
    </div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>