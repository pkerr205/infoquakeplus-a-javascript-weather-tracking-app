<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Geodata Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">
	
	
	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">

	<h1>Geodata</h1>
	
	<p id="title"></p>
	
	<p id="address"></p>
	
	<p id="venues"></p>
	
	
	<script>
	var url_string = window.location.href;
	var url = new URL(url_string);
	var name = url.searchParams.get("name");
	var data1 = url.searchParams.get("data1");
	var data2 = url.searchParams.get("data2");
	console.log(data1);
	console.log(data2);
	
	document.getElementById('title').innerHTML = "Geographical data for <b>" + name + "</b>";
	
	$.ajax({
		url: "https://api.pickpoint.io/v1/reverse/?key=iCkzsW9bqTvmSQsqAXoe&lat=" + data1 + "&lon=" + data2,
		error: function () {
			$('#info').html('<p>An error has occurred</p>');
		},
		success: function (geoData) {
			console.log(geoData);	
			
			var address = "<b>Address</b></br>";
			for (var x in geoData.address) {
			  address = address + x + ": " + geoData.address[x] + "</br>";		  
			}			
			document.getElementById('address').innerHTML = address;			
		}
	});	
	
	$.ajax({
		url: "https://api.foursquare.com/v2/venues/search?ll=" + data1 + "," + data2 + "&client_id=YQYPKRIYNUMWYGRDVX1KULMNI3LU0HIYNSZ2A4S32GZPO0JO&client_secret=NGOL2FCS3AYVLTYHHQMZC5ITWX3G3ZRK5Z4HE4H3GUHC1MVM&v=20140303",
		error: function () {
			$('#info').html('<p>An error has occurred</p>');
		},
		success: function (nearby) {
			console.log(nearby);	
			
			var nearbyVenues = "<b>Nearby Points of Interest</b></br>";
			
			if(nearby.response.venues.length > 0)
			{
				for (var i = 0; i < nearby.response.venues.length; i++) 
				{
					var venueType = "";
					for(var j = 0; j < nearby.response.venues[i].categories.length; j++)
					{
						venueType = venueType + nearby.response.venues[i].categories[j].name;
					}
					nearbyVenues = nearbyVenues + nearby.response.venues[i].name + ", Venue type: " + venueType + "</br>";
				}			
				document.getElementById('venues').innerHTML = nearbyVenues;
			}
			else
			{
				nearbyVenues = nearbyVenues + "No nearby points of interest";			
				document.getElementById('venues').innerHTML = nearbyVenues;
			}
			
		}
	});	
	
	/* $.ajax({
		url: "https://www.mappit.net/bookmap/apis/?lat=" + data1 + "&lon=" + data2 + "&radius=10",
		error: function () {
			$('#info').html('<p>An error has occurred</p>');
		},
		success: function (bookData) {
			console.log(bookData);	
			
			var books = "<b>Books which take place in this location</b></br>";
			for (var x in bookData) {
			  books = books +"Title: " + x.title + ", Author: " + x.author + "</br>";
			}
			
			document.getElementById('books').innerHTML = books;
			
		}
	});	 */
	</script>

	</div>
	
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
	
</body>

</html>