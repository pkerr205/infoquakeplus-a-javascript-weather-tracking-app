<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Annual Pay Changes</title>

		<?php include("partial/_meta"); ?>

		<?php include("partial/_scripts.php"); ?>

		<link rel="stylesheet" type="text/css" href="css/chartStyle.css">
		<script src = "https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script src ="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
	</head>

	<body>

		<div class="d-flex" id="wrapper">
		
		<?php include("partial/_sidebar"); ?>	
		

		<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
		
		<h1> Annual Pay Changes </h1>
		<h3> Please choose a subject to see the annual pay changes from each industry.</h3>

		<input type="button" value="Computing" id="computingBtn" class="btn btn-primary">
		<input type="button" value="Science" id="scienceBtn" class="btn btn-primary">
		<input type="button" value="Law" id="lawBtn" class="btn btn-primary">
		<input type="button" value="Education" id="educationBtn" class="btn btn-primary">
		
		<div id="pay-chartSize">
		<canvas id ="myChart"></canvas>
	    </div>
	   
		<script>
			$(document).ready(function(){
				//-----COMPUTING BUTTON -----
				$('#computingBtn').click(function(){
					
					//ajax call to get the api 
					$.ajax({
						url:"https://api.lmiforall.org.uk/api/v1/ashe/annualChanges?soc=5245",
						type:"get",
						dataType:"json",
						success: function(data){
							// get the length of the anual changes
							var arrComp =[];
							var dataLength = data.annual_changes.length;
							console.log(data.annual_changes[0].change);
							
							// looping the value of change within annual changes
							for(var i = 0; i < dataLength; i++){
								arrComp.push(data.annual_changes[i].change);
							}
							
							console.log(arrComp);
							let myChart = document.getElementById('myChart');
							let statChart = new Chart(myChart, {
								type:'line',
								data:{
									labels:['2012','2013','2014','2015','2016'],
									datasets:[{
										label:'Percentage change in pay in the computing industry over 5 years ',
										backgroundColor: 'rgb(179, 228, 214)',
										data: arrComp
									}]
									},
								options:{}
						
						
							});
							
							
						}
						
						
					})
					
				});
				
			//-- SCIENCE BUTTON ---	
				//-- SCIENCE BUTTON ---
				$('#scienceBtn').click(function(){
					
					//ajax call to get the api 
					$.ajax({
						url:"https://api.lmiforall.org.uk/api/v1/ashe/annualChanges?soc=3119",
						type:"get",
						dataType:"json",
						success: function(data){
							// get the length of the anual changes
							var arrComp =[];
							var dataLength = data.annual_changes.length;
							console.log(data.annual_changes[0].change);
							
							// looping the value of change within annual changes
							for(var i = 0; i < dataLength; i++){
								arrComp.push(data.annual_changes[i].change);
							}
							
							console.log(arrComp);
							let myChart = document.getElementById('myChart');
							let statChart = new Chart(myChart, {
								type:'line',
								data:{
									labels:['2012','2013','2014','2015','2016'],
									datasets:[{
										label:'Percentage change in pay in the science industry over 5 years ',
										backgroundColor: 'rgb(179, 228, 214)',
										data: arrComp
									}]
									},
								options:{}
						
						
							});
							
							
						}
						
						
					})
					
					
				
				});	
				
				
			//---LAW BUTTON ---
			
			
				$('#lawBtn').click(function(){
					
					//ajax call to get the api 
					$.ajax({
						url:"https://api.lmiforall.org.uk/api/v1/ashe/annualChanges?soc=3520",
						type:"get",
						dataType:"json",
						success: function(data){
							// get the length of the anual changes
							var arrComp =[];
							var dataLength = data.annual_changes.length;
							console.log(data.annual_changes[0].change);
							
							// looping the value of change within annual changes
							for(var i = 0; i < dataLength; i++){
								arrComp.push(data.annual_changes[i].change);
							}
							
							console.log(arrComp);
							let myChart = document.getElementById('myChart');
							let statChart = new Chart(myChart, {
								type:'line',
								data:{
									labels:['2012','2013','2014','2015','2016'],
									datasets:[{
										label:'Percentage change in pay in the law industry over 5 years ',
										backgroundColor: 'rgb(179, 228, 214)',
										data: arrComp
									}]
									},
								options:{}
						
						
							});
							
							
						}
						
						
					})
					
					
				
				});	
				
			//--Education button --
			
			$('#educationBtn').click(function(){
					
					//ajax call to get the api 
					$.ajax({
						url:"https://api.lmiforall.org.uk/api/v1/ashe/annualChanges?soc=2318",
						type:"get",
						dataType:"json",
						success: function(data){
							// get the length of the anual changes
							var arrComp =[];
							var dataLength = data.annual_changes.length;
							console.log(data.annual_changes[0].change);
							
							// looping the value of change within annual changes
							for(var i = 0; i < dataLength; i++){
								arrComp.push(data.annual_changes[i].change);
							}
							
							console.log(arrComp);
							let myChart = document.getElementById('myChart');
							let statChart = new Chart(myChart, {
								type:'line',
								data:{
									labels:['2012','2013','2014','2015','2016'],
									datasets:[{
										label:'Percentage change in pay in the education industry over 5 years ',
										backgroundColor: 'rgb(179, 228, 214)',
										data: arrComp
									}]
									},
								options:{}
						
						
							});
							
							
						}
						
						
					})
					
					
				
				});	
			
				
			});
		
		
			
		
		</script>
	
	</body>
	
<html>