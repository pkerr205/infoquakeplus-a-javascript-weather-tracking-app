<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Weather Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.css"/>
<link rel="stylesheet" type="text/css" href="css/chartStyle.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.js"></script>

  <script type="text/javascript" src="https://cdn.aerisjs.com/aeris.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
  
</head>

<body>


	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		<?php include("partial/_header.php"); ?>
		
	
	<h3>Body Mass Index of Both Sexes in the UK (75' - 2016)</h3>
	<div id="select-size">
	<select name="country-select" id="country-select" class="form-control">
		<option value="default" selected="selected">Select a Country</option>
	</select>
	</div>
	<div id="chartSize">
	<canvas id="myChart" width="400" height="400"></canvas>
	</div>

		<script>
	$(function() {
	$.ajax({
	//url: "localdata/bmi.json", local data in case of an emergency!
	url: "https://randomscott.github.io/bmiJson/bmi.json",
	type: "get",
	dataType: "json",
	success: function(bmiData) {
		
		//Declaring arrats for each dataset required for the chart
		var arrBoth = [];
		var arrLabel = [];
		var arrMale = [];
		var arrFemale = [];
		var arrCountry = [];
		
		
		//Length of unfiltered array
		var dataLength = bmiData.fact.length;
		//console.log(bmiData.fact[3]);
		
		for(var j = 0; j < dataLength; j++){
			
			arrCountry.push(bmiData.fact[j].dims.COUNTRY);
		}
		
		var filtCountries = arrCountry.filter(function(elem, index, self) {
		return index === self.indexOf(elem);
		})
		
		
		for(var x = 0; x < filtCountries.length; x++){
			$(document).ready(function(){
				$('#country-select').append("<option>"+ filtCountries[x] +"</option>");
			});
		}
		
		
		document.getElementById("country-select").addEventListener("change", setVals);
		
		//Looping through the array, extracting required data 
		function setVals(){
			
			//clear arrays each time function is called
			var arrBoth = [];
			var arrMale = [];
			var arrFemale = [];
			var arrLabel = [];
			
			//put selected option in variable
			var selectedCountry = $(this).find('option:selected').text();
			
			for(var i = dataLength - 1;i > 0; i-=1){
				if(bmiData.fact[i].dims.COUNTRY == selectedCountry && bmiData.fact[i].dims.SEX == "Both sexes"){
					//Pushing found data to array
					arrBoth.push(parseFloat(bmiData.fact[i].Value));
					
					/*
					Pushing to arrLabel only has to be done on the first if 
					statement since if the year is in all, then it exists
					for the other two. Doing so in the other statements would make
					duplicates.
					*/
					arrLabel.push(bmiData.fact[i].dims.YEAR);
					//console.log(bmiData.fact[i]);
				}
				
				if(bmiData.fact[i].dims.COUNTRY == selectedCountry && bmiData.fact[i].dims.SEX == "Male"){
					arrMale.push(parseFloat(bmiData.fact[i].Value));
					//console.log(bmiData.fact[i]);
				}
				
				if(bmiData.fact[i].dims.COUNTRY == selectedCountry && bmiData.fact[i].dims.SEX == "Female"){
					arrFemale.push(parseFloat(bmiData.fact[i].Value));
					//console.log(bmiData.fact[i]);
	
					
				}
			}	
				//destroy chart
				if(window.chart && window.chart !== null){
					window.chart.destroy();
				}
				
				//create new passing appropriate information as parameters 
				loadChart(arrBoth, arrMale, arrFemale, arrLabel);
				
				
		}
		
	function loadChart(bothData, maleData, femaleData, chartLabels){

				var ctx = document.getElementById('myChart').getContext('2d');
				ctx.canvas.width = 1660;
				ctx.canvas.height = 666;
				
				window.chart = new Chart(ctx, {
					type: 'line',
					  data: {
						labels: chartLabels,
						datasets: [{ 
							data: bothData,
							label: "Both Sexes",
							borderColor: 'rgba(62, 150, 81, 1)',
							fill: false
						  },
						  { 
							data: maleData,
							label: "Male",
							borderColor: 'rgba(56, 106, 177, 1)',
							fill: false
						  },
						  { 
							data: femaleData,
							label: "Female",
							borderColor: 'rgba(204, 37, 41, 1)',
							fill: false
						  },
						]
					  },
					options: {
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true,
									min: 12,
									max: 35,
								}
							}]	
						},
						maintainAspectRatio: false
					}
				});
				
				}
			}
		});
	});
		</script>
	
  
  
	
	</div>
	
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>