<!DOCTYPE HTML>
<html lang="en">
<head>
<title>News Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">News Tutorial</h1>
			<h3>Where the data was sourced.</h3>
			<p> The data for the News page was taken from the <a href ="https://developer.nytimes.com/">New York Times.</a> This data was displayed in JSON format and was extracted by using AJAX and jQuery. </p>
			
			<h3>How the data was obtained.</h3>
			<p>The data was want specified as they user can search anything. However, it is possible to narrow your search.For more information about this visit <a href ="https://developer.nytimes.com/docs/articlesearch-product/1/overview" >here</a>. To call the news api we used AJAX and jQuery to extract this data to display.</p>
			<h3>How the data was visualised.</h3>
			<p>To visualise this data we used a word cloud. It asks the user to enter what they would like to reseach then returns a word cloud of related searched terms and the most relevent in the bigger text.
			This visualisation is a great way to see what trending topics are. To create a word cloud the term searched by the user, then search the api for related terms, then these terms are rounded up into the word cloud using the size of text to display the most relevent related to the topic. 
			The library used to create the word cloud was wordcloud2.js.</p>
	
			<h3>More information on wordcloud2.js</h3>
			<p>For more information on using wordcloud2.js <a href "https://github.com/timdream/wordcloud2.js/blob/gh-pages/API.md" >click here</a>.</p>
		
			
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>