<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Hurricanes Page</title>


<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.js"></script>

<script type="text/javascript" src="https://cdn.aerisjs.com/aeris.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

<style>
	#map-canvas {
	  width: 800px;
	  height: 600px;
	}
</style>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	
  


</body>

	<div id="page-content-wrapper">

	
	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">

	<div id="map-canvas"></div>
	
	</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
	<script type="text/javascript">
    // Configure Aeris API keys
    aeris.config.set({
      apiId: 'WKkVoFhlzpHaqBaVcBLRS',
      apiSecret: 'TpXNPQsTkMO8eSglCf1tcrXE9qSPQnC6MqI8y98y'
    });
    
    // Create the map, where 'map-canvas' is the id of an HTML element.
    var map = new aeris.maps.Map('map-canvas', {
      zoom: 3,
      center: [49.6107,25.1367],
      baseLayer: new aeris.maps.layers.AerisTile({
        tileType: 'flat-dk',
        zIndex: 1
      })
    });
    
    // Create 'water-depth' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'water-depth',
      zIndex: 3,
      map: map
    });

    // Create 'wind-dir' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'wind-dir-dk',
      zIndex: 12,
      map: map
    });

    // Create 'wind-speeds-text' layer
    new aeris.maps.layers.AerisTile({
      tileType: 'wind-speeds-text-dk',
      zIndex: 14,
      map: map
    });
  </script>
	
</body>
</html>