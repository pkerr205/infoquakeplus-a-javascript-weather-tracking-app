<!DOCTYPE HTML>
<html lang="en">
<head>
<title>GeoJSON Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">GeoJSON Tutorial</h1>
			<h3>What is GeoJson</h3>
			<p>The GeoJSON data format is often used to simply represent geographic features such as continents, and their non-space related attributes. It's used to in our Earthquake page and our Weather page. Its used in conjuntion with JavaScript and allows data to be stored in a text based format. Most programming languages can use GeoJSON if they have tha ability to read and write JSON data.</p>
			<h3>How is GeoJSON used?</h3>
			<p> GeoJSON as stated before is used in our weather and earthquake pages, this is used when display data on a map. Some element when working with geographical data is using coordinate, position, geometry, polygons etc, which are all elements that GeoJSON supports. For a more indepth explination about GeoJSON <a href "https://macwright.org/2015/03/23/geojson-second-bite">click here.</a>
			</p>
			
			<p>This video is a helpful guide into learning about GeoJSON.</p>
			<iframe width="40%" height="70%" src="https://www.youtube.com/embed/8RPfrhzRw2s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			
		
			
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>
