<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Pollution Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
<link  rel="stylesheet"  href="http://cdn.leafletjs.com/leaflet-0.7.5/leaflet.css"  />  
<script  src="http://cdn.leafletjs.com/leaflet-0.7.5/leaflet.js"></script> 
<style>
.center {
  text-align: center;
  border: 3px solid green;
}
.center2 {
  text-align: center;
}
</style>
</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">
	
	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">
	
	<div class="center2"> <h3>Google Maps AQI Markers</h3> <br>
	

	
	
	  
  	<div id='map' style='height:500px;'  /> 
	
	
	  
	
	  
	<script>  
			function initMap(){
				//create map
					var map = new google.maps.Map(document.getElementById('map'),{  
						center: new  google.maps.LatLng(55.833, -4.09),  
						mapTypeId: 'satellite',  
						zoom: 11  
					});  
					
					//create overlay for the map
					//var t = new Date().getTime();  
					var waqiMapOverlay = new google.maps.ImageMapType({  
						getTileUrl: function(coord, zoom){  
							return 'https://tiles.waqi.info/tiles/usepa-aqi/' + zoom + "/" + coord.x + "/" + coord.y + ".png?token=_TOKEN_ID_";  
						  },  
						  name:"Air Quality",  
					});  
					//add overlay to the map
				map.overlayMapTypes.insertAt(0,waqiMapOverlay); 
			}	  
	</script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDX_pImCjOZn_IBH8lK7AHFpf3_UIZNVNA&callback=initMap">
    </script>
	</div>
	</div>
	<br><br>
	<div class="center"> <h5>Search for a Capital cities AQI!</h5>
	<p>Enter a capital city below to display detailed information: </p>
	<span id="city-aqi-container"></span>
	<form action="#" onsubmit="widgetCall()">
      <input type="text" placeholder="London, Paris ect" id="search">
      <button type="submit" class="btn btn-primary">Search</button>
	  <script  type="text/javascript"  charset="utf-8">  
		(function(w,d,t,f){  w[f]=w[f]||function(c,k,n){
				s=w[f],k=s['k']=(s['k']||(k?('&k='+k):''));
				s['c']= c=(c  instanceof  Array)?c:[c];
				s['n']=n=n||0;
				L=d.createElement(t), e=d.getElementsByTagName(t)[0];  
				L.async=1;
				L.src='//feed.aqicn.org/feed/'+(c[n].city)+'/'+(c[n].lang||'')+'/feed.v1.js?n='+n+k;  
				e.parentNode.insertBefore(L,e);
			};
		})
		(window,document,'script','_aqiFeed'); 

	function widgetCall(){
				
			var cityInput = (document.getElementById("search"));
			//console.log(cityInput.value);
				_aqiFeed({ display:"%details", container:"city-aqi-container",  city:cityInput.value, 				}); 

			}	
	</script>
</div>
    </form>
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>