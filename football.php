<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Football Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>
<link rel="stylesheet" type="text/css" href="css/chartStyle.css">

</head>



<body>


	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">

	<h1>Football</h1>
	
	<p>Select a league and teams to create a graph of recent head to head results</p>
		
	<p>&nbsp;</p>
	
	<div id="leftbox">
	
	<div id="" class="row">
		<div id="" class="col-sm-12 col-md-4">
		<h3 class="select-heading" align="center">League</h3>
			<select name="league-select" id="league-select" class="form-control">
				<option value="default" selected="selected">Select a League</option>
			</select>
		</div>	
		
		<div id="" class="col-sm-12 col-md-4">
		<h3 class="select-heading" align="center">Home</h3>
			<select name="team1-select" id="team1-select" class="form-control">
				<option value="default" selected="selected">Select Home Team</option>
			</select>
		</div>
		
		<div id="" class="col-sm-12 col-md-4">
		<h3 class="select-heading" align="center">Away</h3>
			<select name="team2-select" id="team2-select" class="form-control">
				<option value="default" selected="selected">Select Away Team</option>
			</select>
		</div>
		
	<p>&nbsp;</p>
	
		<div id="" class="col-sm-12">
			<button id="show-score-graph" class="btn btn-primary">Display score graph</button>
		</div>
		
	</div>
	</div>	
	
		
	<h2 id="team-names" align="center"></h2>
		
	<div id="chart-container">	
		<div id="chart" style="min-height: 364px;"></div>
	</div>
	
	<script>
	
		var team1 = "";
		var team2 = "";
		var matches = new Array();
		var homeScores =new Array();
		var awayScores = new Array();
		var matchDates = new Array();
		var maxScore = 0;
		var teamNames = document.getElementById('team-names');
		var chartdiv = document.getElementById('chart');
		
		
		var teamArray = new Array();
		
	
		var leagueArray = new Array();	
		$.ajax({
			 url: "https://www.thesportsdb.com/api/v1/json/1/all_leagues.php",
			 type: "get",
			 dataType: "json",
			 success: function(leagues)
			 {	
			 console.log(leagues);
				var select = document.getElementById("league-select");

				for( var i = 0; i < 22/*leagues.leagues.length*/; i++)
				{
					if(leagues.leagues[i].strSport == "Soccer")
					{
						select.options[select.options.length] = new Option(leagues.leagues[i].strLeague, i);
						leagueArray.push(leagues.leagues[i]);
					}
				}	
						console.log(leagueArray);
			 }
		});
		
		
		$('#leftbox').on('change','#league-select',function() {
			var selectedLeague = $("#league-select").val();
			if(selectedLeague != "default")
			{
				$.ajax({
					 url: "https://www.thesportsdb.com/api/v1/json/1/lookup_all_teams.php?id=" + leagueArray[selectedLeague].idLeague,
					 type: "get",
					 dataType: "json",
					 success: function(listTeams)
					{	
						
						teamArray = [];
						var select1 = document.getElementById("team1-select");
						var select2 = document.getElementById("team2-select");
						
						$(select1).empty();
						$(select1).append('<option value="default" selected="selected">Select Home Team</option>');
						$(select2).empty();
						$(select2).append('<option value="default" selected="selected">Select Away Team</option>');

						for( var i = 0; i < listTeams.teams.length; i++)
						{
							select1.options[select1.options.length] = new Option(listTeams.teams[i].strTeam, i);
							select2.options[select2.options.length] = new Option(listTeams.teams[i].strTeam, i);
							teamArray.push(listTeams.teams[i]);
						}			
							
					}		
				});						
			}
		});	
		
		
		
		$('#leftbox').on('click','#show-score-graph',function() {
			
			matches = [];
			homeScores = [];
			awayScores = [];
			matchDates = [];
			teamNames.innerHTML = '';
			
			 $('#chart').remove();
			 $('#chart-container').append('<div id="chart" style="min-height: 364px;"></div>');
			
			
			team1 = teamArray[$("#team1-select").val()].strTeam;
			team2 = teamArray[$("#team2-select").val()].strTeam;
		
			$.ajax({
				 url: "https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=" + team1 + "_vs_" + team2,
				 type: "get",
				 dataType: "json",
				 success: function(data)
				{		
					
					if(data.event != null)
					{
						var selection = team1 + " vs " + team2;
						teamNames.innerHTML = selection;
						
						for( var i = data.event.length - 1; i >= 0; i--)
						{					
							
							var dateFormat = moment(data.event[i].eventDate).format('DD/MM/YYYY');
							
							var match = {
								home: data.event[i].intHomeScore, 
								away: data.event[i].intAwayScore, 
								matchdate: moment(data.event[i].dateEvent).format('DD/MM/YYYY')
								};
							matches.push(match);					
						}	
						
										

						matches.sort(function(a, b){
							var aa = a.matchdate.split('/').reverse().join(),
								bb = b.matchdate.split('/').reverse().join();
							return aa < bb ? -1 : (aa > bb ? 1 : 0);
						});


							
						for( var j=0; j < matches.length; j++)
						{	
							homeScores.push(matches[j].home);
							if(matches[j].home > maxScore)
							{
								maxScore = matches[j].home;
							}
							awayScores.push(matches[j].away);
							if(matches[j].away > maxScore)
							{
								maxScore = matches[j].away;
							}
							matchDates.push(matches[j].matchdate);
						}	
						
						options = {
							chart: {
								height: 350,
								type: 'bar',
								stacked: true,
								shadow: {
									enabled: true,
									color: '#000',
									top: 18,
									left: 7,
									blur: 10,
									opacity: 1
								},
								toolbar: {
									show: false
								}
							},
							colors: ['#77B6EA', '#545454'],
							dataLabels: {
								enabled: true,
							},
							stroke: {
								curve: 'smooth'
							},
							series: [{
									name: team1 + " Goals",
									data: homeScores
								},
								{
									name: team2 + " Goals",
									data: awayScores
								}
							],
							title: {
								text: 'Results for ' + team1 + " playing at home against " + team2,
								align: 'center'
							},
							grid: {
								borderColor: '#e7e7e7',
								row: {
									colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
									opacity: 0.5
								},
							},
							markers: {
								
								size: 6
							},
							xaxis: {
								categories: matchDates,
								title: {
									text: 'Month'
								}
							},
							yaxis: {
								title: {
									text: 'Goals'
								},
								min: 0,
								max: parseInt(maxScore) + 1
							},
							legend: {
								position: 'top',
								horizontalAlign: 'right',
								floating: true,
								offsetY: -25,
								offsetX: -5
							}
						}
						
						var chart = new ApexCharts(
							document.querySelector("#chart"),
							options
						);
						chart.render();
										
					}
					else 
					{
					
					}
					
				}
			})
		
		
		})
					
	</script>
	
	
	
	</div>
		

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>