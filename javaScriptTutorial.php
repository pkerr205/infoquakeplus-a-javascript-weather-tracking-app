<!DOCTYPE HTML>
<html lang="en">
<head>
<title>JavaScript Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">JavaScript Tutorial</h1>
			
			<h3>Background of JavaScript</h3>
			<p>JavaScript is used to create dynamic and interacrtive websites. It's ran in the browser meaning it is a client side language. It is easy to learn and is widley used for web development and design.</p>
		
			<h3>How Is JavaScript Incorporated Into InfoQuake+</h3>
			<p>For our website to be dynamic and interacive for the user we use JavaScript. JavaScript is used to access the data from API's. With the data on our site coming from API's we heavily used AJAX.</p>
			
			<h3>What is AJAX and how can I use it?</h3>
			<p>AJAX stands for Asynchronous JavaScript, it is a technique used by many web developers to create dynamic websites. A dynamic website that can display data from a external source which does not need to be reoaded. An AJAX call looks like this:</br>
			<b>$.ajax({</b>
						</br><b>url:"Insert api url here",</b> --- <i>this identifys what data is to be used.</i>
						</br><b>type:"get",</b> --- <i>this means that data will be extracted from the api.</i>
						</br><b>dataType:"json",</b> --- <i>this is the format of the api another data type can be xml.</i>
						</br><b>success: function(data){}</b> --- <i>this is if the api is acessed successfuly to carry out the function.</i>
			<br><br>XMLHttpRequest is an object that allow data to be requested from a web server, Allowing it to be updated without reloading the page, request data and recieve data after the page has loaded and send data to the server while the page is running. </p>
			
			<h3>What is jQuery?</h3>
			<p>jQuery is a JavaScript library which can be used in conjunction with AJAX. JQuery makes AJAX easier, it uses HTTP Get and HTTP Post to access JSON and XML etc. from a remote server. </p>
			
			<h3>Learn more about at W3Schools</h3>
			<p>Learn more about<a href ="https://www.w3schools.com/js/default.asp"> Javascript.</a></br>
			Learn more about<a href ="https://www.w3schools.com/xml/ajax_intro.asp"> AJAX.</a></br>
			Learn more about<a href ="https://www.w3schools.com/jquery/default.asp"> jQuery.</a><p>
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>