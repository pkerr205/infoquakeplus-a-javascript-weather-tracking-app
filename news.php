<!DOCTYPE HTML>
<html lang="en">
<head>
<title>News Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/chartStyle.css">

</head>

<body>


	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">

	<h1>News</h1>
	
	<p>Enter a search term to create a word cloud of keywords from the top NY Times news stories which feature the keyword.</p>
	
	<div id="leftbox" class="col-sm-5">
		<form method="post">
			<div class="form-group input-group">
				<input id="search-box" class="form-control" type="text" name="search-box" placeholder="Enter search term..">
			<button id="search-submit" class="btn btn-primary"  type="submit" name="submit">Search</button>
			</div>
		</form>
	</div>
	
	<div id="surrounding_div" style="width:100%;height:500px">
		<canvas id="word_cloud" class="word_cloud"></canvas>
	</div>
	
	<script>	
		
	 $('#leftbox').on('click','#search-submit',function(e) 
	{	
		var wordArray = {};
		var list = [];
		e.preventDefault();
		var searchTerm = document.getElementById("search-box").value;	
		var searchUrl = "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=" + searchTerm + "&api-key=YA3MK1pRByCYWM4euAT5efd1MZe8gBaT";
		console.log(searchUrl);
		
		$.ajax({
		 url: searchUrl,
		 type: "get",
		 dataType: "json",
		 success: function(data)
		 {
			
			for( var i = 0; i < data.response.docs.length-1; i++ ) {
				
				for( var j = 0; j < data.response.docs[i].keywords.length-1; j++ ) {
					
					var keyword = data.response.docs[i].keywords[j].value;
					
					if(wordArray[keyword])
					{
						wordArray[keyword] = wordArray[keyword] + 12;
					}						
					else
					{
						wordArray[keyword] = 12;
					}
				}				
			}
			
			list = Object.entries(wordArray);
			
			console.log(list);
			
			var div = document.getElementById("surrounding_div");

			var canvas = document.getElementById("word_cloud");

			canvas.height = div.offsetHeight;

			canvas.width  = div.offsetWidth;

			WordCloud(document.getElementById('word_cloud'), 
			{ 
				list: list,
				fontFamily: 'Merriweather, serif',
				click: function(item) 
				{
					alert(item[0] + ': ' + (item[1]/12));
				}
			});	
			
		 }
		});  
	});
		
	</script>
	
	
	
	</div>
		

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>