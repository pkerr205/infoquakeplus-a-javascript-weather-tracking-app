<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Pay Rise Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">Pay Rises Tutorial</h1>
			<h3>Where the Data was sourced.</h3>
			<p>The data for this page was taken from <a href ="http://api.lmiforall.org.uk/">lmiforall.org.uk.</a> This site gives data on educations and job data it is run by the department of ediucation. </p>
			<h3>How the data was obtained.</h3>
			<p>To obtain the data on the pay rises we used a JSON API that was catogoriesd for soc code for each job area. This allowing to find various infomation on a specific catorgory. Using AJAX and jQuery to extract this data to display.  </p>
			<h3>How is was the data visualised.</h3>
			<p>To visuslise the data we used a line chart from the chart.JS library. As the data shows the rise and fall in percentage of the changes to the pay in that sector we felt this was the best chart to display this. Chart.js is a simple and easy to use open source library that allows developers to use to display different types of charts using javascript.
			<br> To dispaly the data for each job area it allows the user to select from a button. From there the apporpriate call is made to the corresponding api. When it is sucessful it will display the percentage of change and the year it corresponds to.</p>
			<h3>Learn more about Chart.JS.  </h3>
			<p>This is a link to the<a href ="https://www.chartjs.org/docs/latest/charts/line.html"> Chart.js</a> website. </p>
			
			
		
			
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>