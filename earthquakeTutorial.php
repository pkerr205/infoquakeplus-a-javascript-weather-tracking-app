<!DOCTYPE HTML>
<html lang="en">
<head>
<title> Earthquake Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">Earthquake Tutorial</h1>
			</br>
			
			<h3>About Earthquakes</h3>
			<p>Earthquakes are characterised as a sudden shaking of the earth caused by movements in the earths lower crust. The earths crust covers everywhere even the ocean's floor so earthquakes could occur anywhere. However they're significantly more likely over certain areas in the world, where the crust's movement is more common, these areas are often called faults or fault-lines. Earthquakes that occur in the ocean can cause tsunamis, however this is not always the case. 
			</br>Earthquakes are measured using a scale called  "magnitude" and are classed from "Minor" to "Great" according to their magnitude. The magnitude is the strength of the earthquake. Earthquakes with a magnitude bellow 3 aren't classed as minor but can be detected using an earthquake monitoring device "seismograph", and while they're the most common on average people usually can't even feel them.  </p>
		
			<h3>How We Source Earthquake Data </h3>
			<p>For our website we sourced the earthquake data from USGS. They provide information on earthquake science. For more information on USGS <a href ="https://earthquake.usgs.gov/aboutus/">click here</a>.</p>
			
			<h3>How to Collect and Display Earthquake Data</h3>
			<p>To display up to date data we use API's which allow to present dynamic data. To collect the data we used the data taken from the USGS website which displays in a json format. To display this data we use Ajax. Ajax is used to access web servers where it allows yout to update a web paged without refreshing the page, request, recieve and send data to and from a server.  </p>
			
			<h3>How to Visualise Earthquake Data</h3>
			<p>To display the earthquakes that are in close proximity to each other we used the <a href = "https://developers.google.com/maps/documentation/javascript/marker-clustering">marker clusting library made available by google maps</a>. Using the maping clusters in conjunction with Googles maps JavaScript API allows to display the data on a map with the earthquakes in the exact loaction. </p>
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>