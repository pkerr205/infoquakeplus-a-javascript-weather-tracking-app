<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
	<button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
		<li class="nav-item active">
		  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
		</li>
		<li class="nav-item dropdown">
		  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Data Pages</a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="earthquakes.php" class="list-group-item list-group-item-action bg-light">Earthquakes</a>
				<!--<a class="dropdown-item" href="hurricanes.php" class="list-group-item list-group-item-action bg-light">Hurricanes</a>
				<a class="dropdown-item" href="pollution.php" class="list-group-item list-group-item-action bg-light">Pollutions</a>
				<a class="dropdown-item" href="population.php" class="list-group-item list-group-item-action bg-light">Population</a>
				<a class="dropdown-item" href="tsunamis.php" class="list-group-item list-group-item-action bg-light">Tsunamis</a>-->
				<a class="dropdown-item" href="weather.php" class="list-group-item list-group-item-action bg-light">Weather</a>
				<a class="dropdown-item" href="news.php" class="list-group-item list-group-item-action bg-light">News</a>
				<a class="dropdown-item" href="football.php" class="list-group-item list-group-item-action bg-light">Football</a>
				<a class="dropdown-item" href="bmi.php" class="list-group-item list-group-item-action bg-light">Body Mass Index</a>
				<a class="dropdown-item" href="death.php" class="list-group-item list-group-item-action bg-light">Average Life Expectancy</a>
			 </div>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="authors.php">Authors</a>
		</li>
		
		<li class="nav-item dropdown">
		  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Tutorials</a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="earthquakeTutorial.php" class="list-group-item list-group-item-action bg-light">Earthquake Tutorial</a>				
				<a class="dropdown-item" href="weatherTutorial.php" class="list-group-item list-group-item-action bg-light">Weather Tutorial</a>
				<a class="dropdown-item" href="javaScriptTutorial.php" class="list-group-item list-group-item-action bg-light">JavaScript Tutorial</a>
				<a class="dropdown-item" href="GeoJsonTutorial.php" class="list-group-item list-group-item-action bg-light">GeoJSON Tutorial</a>
				<a class="dropdown-item" href="bmiTutorial.php" class="list-group-item list-group-item-action bg-light">Body Mass tutorial</a>
				<a class="dropdown-item" href="lifeExpTutorial.php" class="list-group-item list-group-item-action bg-light">Average Life Expectancy Tutorial</a>
				<a class="dropdown-item" href="payChangesTutorial.php" class="list-group-item list-group-item-action bg-light">Pay Rises Tutorial</a>
				<a class="dropdown-item" href="footballTutorial.php" class="list-group-item list-group-item-action bg-light">Football Tutorial </a>
				<a class="dropdown-item" href="newsTutorial.php" class="list-group-item list-group-item-action bg-light">News Tutorial </a>
			 </div>
		</li>
	  </ul>
	</div>
</nav>