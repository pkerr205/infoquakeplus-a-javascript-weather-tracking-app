<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Average Life Expectancy Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">Average Life Expectancy Tutorial</h1>
			<h3>Where the Data was sourced.</h3>
			<p>The data was taken from undata. They provide a large range of APIs that you can use from population, education and crime.</p>
			<h3>How was the data obtained </h3>
			<p> To obtain this data we used AJAX and jQuery. The API was returned in a JSON format. </p>
			<h3>How was the data visualised.</h3>
			<p>The data is then visualised using a bar chart from the library chart.js. The data is sorted between male and feamle average life expectancy in that counrty selected by the user. The user is able to select the country they would like to see the data for by a drop down box with all the countries avaliable./p>
			<h3>Learn more about Chart.JS.  </h3>
			<p>This is a link to the<a href ="https://www.chartjs.org/docs/latest/charts/bar.html"> Chart.js</a> website. </p>
	
		
			
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>