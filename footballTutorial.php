<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Football Tutorial </title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		
		<?php include("partial/_header.php"); ?>
		
		<div class="container-fluid">
			<h1 class="mt-4">Football Tutorial</h1>
			<h3>Where the Data was sourced.</h3>
			<p>The data for the football page was sourced from <a href ="https://www.thesportsdb.com/">the sportsdb.</a> </p>
			<h3> How was the data obtained</h3>
			<p>The data was obtained in a JSON format using AJAX and jQuery. The API's used include the leagues and the teams. We used an array to search through the teams and leagues.</p>
			<h3>How the data is visualised </h3>
			<p>The data was visualised by using apex charts. which is an open source library to provideds a range of different chart types to show data. For the football page we uses a stacked barchart to show the scores between the both teams in the league.
			The football page allow the user to choose the league then the 2 teams they would like to compare, once the user has choosen the chart will return the corresponding data taken from the API's. </p>
			<h3>Learn more about ApexCharts </h3>
			<p>For more information on ApexCharts <a href ="https://apexcharts.com/">click here</a>.</p>
	
		
			
			
		</div>

	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>