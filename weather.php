<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Weather Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.css"/>
<link rel="stylesheet" href="css/result.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.2/leaflet.js"></script>
  <script type="text/javascript" src="https://cdn.aerisjs.com/aeris.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

</head>

<body>


	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">

		<?php include("partial/_header.php"); ?>
		
		
		<style>
		  /* Set the size of the div element that contains the map */
		  #map {
			height: 400px;
			width: 100%;
		   }
		</style>
  </style>

		<div class="container-fluid">
		
		<h1>Temperature Forcast (Metric)</h1>
		
		<h3>Interactive Google Maps Weather Tool</h3>
		<p>Type a place name into the search box or drag the marker to get current and future weather info</p>
		<!--The div element for the map -->
		<div id="top-stuff">
			<div id="map-pad" class="col-sm-8">
			  <div id="map"></div>
			</div>
			<div id="search-container" class="col-sm-4">
				<div id="search-group">
					<form method="post">
					  <div class="form-group input-group">
						<input id="search-box" class="form-control" type="text" name="search-box" placeholder="Enter search term..">
						<button id="search-submit" class="btn btn-primary"  type="submit" name="submit">Search</button>
					  </div>
					</form>	
				</div>
				
				<div class="padding">
					<p class="cast-date" id="curr-date-label">Date:</p>
					<p class="cast-date" id="curr-date-0">  . . . </p></br></br>
					<p class="cast-max-temp" id="curr-temp-label"> Current Temp(c):</p>
					<p class="cast-max-temp" id="curr-temp-0">     . . . </p></br></br>
					<p class="cast-condition" id="curr-cond-label">condition:</p>
					<p class="cast-condition" id="curr-cond-0">     . . .</p></br></br>
					<div class="imagePos" id="curr-weatherImage"></div>
			</div>
			
			</div>
			<div id="weatherImage"></div>
		</div>
			
			
		
		<div id="forecast-section">
		</br>
		<h3>Forecast Tool</h3>
		<h4 id="cast-location"></h4>
			
		
		
		
		<div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-0" >
			<div class="imagePos" id="weatherImage-0"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-0">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-0">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-0">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-0">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  <div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-1">
			<div class="imagePos" id="weatherImage-1"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-1">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-1">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-1">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-1">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  <div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-2">
			<div class="imagePos" id="weatherImage-2"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-2">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-2">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-2">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-2">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  <div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-3">
			<div class="imagePos" id="weatherImage-3"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-3">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-3">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-3">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-3">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  <div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-4">
			<div class="imagePos" id="weatherImage-4"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-4">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-4">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-4">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-4">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  <div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-5">
			<div class="imagePos" id="weatherImage-5"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-5">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-5">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-5">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-5">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  <div id="bottom-container">
		  <div class="forecast-container col-sm-4" id="cntr-6">
			<div class="imagePos" id="weatherImage-6"></div>
		  <div class="padding">
			<p class="cast-date" id="date-label">Date:</p>
			<p class="cast-date" id="date-6">  . . . </p>
			<p class="cast-avg-temp" id="avg-label">Avg Temp(c):</p>
			<p class="cast-avg-temp" id="avg-6">     . . .</p>
			<p class="cast-max-temp" id="max-label">Max Temp(c):</p>
			<p class="cast-max-temp" id="max-6">     . . . </p>
			<p class="cast-condition" id="cond-label">condition:</p>
			<p class="cast-condition" id="cond-6">     . . .</p>
			</div>
		  </div>
		  </div>
		  
		  
		  
		  </div>
		</div>
</div>
		
		<script>
		// Initialize and add the map
		function initMap() {
		// The location of Glasgow
		var glasgow = {lat: 55.873, lng: -4.289};
		  
		// The map, centered at Glasgow
		var map = new google.maps.Map(
		document.getElementById('map'), {zoom: 4, center: glasgow}
		);
			  
		// The marker, positioned at Glasgow by default
		var marker = new google.maps.Marker({
			position: glasgow, map: map, draggable:true, title: "Drag me to location to find out its weather!"
			});
			
		$("#lat").text(glasgow.lat);
		$("#long").text(glasgow.lng);
		
		//Event listner for when the marker is dragged
		google.maps.event.addListener(marker, "dragend", function(event) { 
			
			
			//setting the latitude and longitude coords
			var lat = event.latLng.lat(); 
			var lng = event.latLng.lng();
			
			$("#lat").text(lat.toFixed(5));
			$("#long").text(lng.toFixed(5));
			
	
			
			/*Printing to console 
			console.log(lat);
			console.log(lng);
			*/		
			
			//Bounce animation for the marker
			marker.setAnimation(google.maps.Animation.BOUNCE);
			setTimeout(function(){ marker.setAnimation(null); }, 150);
			map.setCenter({lat: lat, lng: lng}); 
			
			
				
				//API url with the lat and lng appended to it after being converted to type: string 
				theURL = 'https://api.apixu.com/v1/current.json?key=5393a434351743b9be8172532190303&q=' + lat.toFixed(4) + ',' + lng.toFixed(4);
				$.ajax({
					url: theURL,
					success: function (data) {


						image = new Image();
						//Error handling. Blank image if error occurs with api call
						if (data.error) {
							image.src = "http://via.placeholder.com/64x64?text=%20"; 
						}
						else {
							//finding appropriate icon
							image.src = "http:" + data.current.condition.icon; 

							//Appending text for the weather info
							$('#weatherInfo').html('<p>' + data.current.condition.text + '</p>'); 
							
						}
							//Appending the appropriate icon to the image div
							image.onload = function () {
							//$('#weatherImage').empty().append(image); old image box
							setInfoBoxPic();

						};
						
						function setInfoBoxPic(){
							
							
							google.maps.InfoWindow.prototype.opened = false;
							var contentString = '<p>' + data.current.condition.text + '</p></br>';

							var infowindow = new google.maps.InfoWindow({
							content: image
								});	
								
								/* The logic in this section is fine, but there is
								another issue that I can't solve. Info windows will
								pop up ontop of each other. Current fix is to set 
								a close timer for each infowindow since for some 
								reason it can't be 
								
								*/
								/* if($(".gm-style-iw-d").length){									
									 setTimeout(function(){
										infowindow.close();
										}, '1000');	 
									console.log("1");
									infowindow.open(map, marker);
								
								}else{
									console.log("2");
									
								infowindow.open(map, marker);
								//setTimeout(function(){infowindow.close();}, '5000');
								
								}  */
								
								
								

							infowindow.open(map, marker);
							setTimeout(function(){infowindow.close();}, '5000');							
						}
						
						
			
						
					},
					//Error handling. In case the API fails. Appends a blank square to the image div
					error: function () { 
						image = new Image();
						image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAAPElEQVR42u3OMQEAAAgDIJfc6BpjDyQgt1MVAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBgXbgARTAX8ECcrkoAAAAAElFTkSuQmCC";
						image.onload = function () {
							$('#weatherImage').empty().append(image);
						};
						
					}
				});
				getForecast(lat,lng);
			});
		
		
		
		}
		</script>
		
		<script>
		$('#search-container').on('click','#search-submit',function(e) 
	{	
		e.preventDefault();
		var searchString = document.getElementById("search-box").value;
		var searchTerm = searchString.replace(/\s/g, '') ;
		getCoord(searchTerm);
		  
	});
		
		
		
		function getCoord(searchTerm){
			var arrCoord = searchTerm.split(',');
			
			var la = arrCoord[0];
			var lo = arrCoord[1];;
			getForecast(la, lo);
		}
		
		
		
		function getForecast(lat, lng){
			//API url with the lat and lng appended to it after being converted to type: string 
				apiRequest = 'https://api.apixu.com/v1/forecast.json?key=5393a434351743b9be8172532190303&q='+ lat + ',' + lng +'&days=8';
				$.ajax({
					url: apiRequest,
					success: function (data) {
						//Declaring variables for current weather
						var currDate = data.current.last_updated;
						var currTemp = data.current.temp_c;
						var currCondition = data.current.condition.text;
						var currImage = data.current.condition.icon;
						var currImgObj = new Image();
						
						//append data to 'current' elements
						$('#curr-date-0').text(currDate);
						$('#curr-temp-0').text(currTemp);
						$('#curr-cond-0').text(currCondition);
						currImgObj.src = "http:" + currImage; 
						currImgObj.onload = function () {
							$('#curr-weatherImage').empty().append(currImgObj);
						};
						
						
						//Declaring array for each days forcast
						var arrLocation = [];
						var arrDate = [];
						var arrAvgTemp = [];
						var arrMaxTemp = [];
						var arrCondition = [];
						var arrImage = [];
						
						//traversed data to forecast
						var obj = data.forecast.forecastday;
						console.log(data);
						
						//loop to fill the arrays with data from the API
						var i;
						for(i = 0; i < obj.length; i++){
							arrLocation.push(data.location.region + ', ' + data.location.country);
							arrDate.push(obj[i].date);
							arrAvgTemp.push(obj[i].day.avgtemp_c);
							arrMaxTemp.push(obj[i].day.maxtemp_c);
							arrCondition.push(obj[i].day.condition.text);
							arrImage.push(obj[i].day.condition.icon);
						}
						
						//declaring variables/objects for forecast image
						image0 = new Image();
						image1 = new Image();
						image2 = new Image();
						image3 = new Image();
						image4 = new Image();
						image5 = new Image();
						image6 = new Image();
						image7 = new Image();
						
						
						//loop for appending elements with data
						for(i = 0; i < obj.length; i++){
							$('#cast-location').text(arrLocation[i]);
							$('#date-'+i).text(arrDate[i]);
							$('#avg-'+i).text(arrAvgTemp[i]);
							$('#max-'+i).text(arrMaxTemp[i]);
							$('#cond-'+i).text(arrCondition[i]);
							//$('#weatherImage-'+i).text('http:' + arrImage[i]);
							
								switch(i) {
								  case 0:
									image0.src = "http:" + obj[i].day.condition.icon; 
									image0.onload = function () {
									$('#weatherImage-0').empty().append(image0);
									};
									break;
								  case 1:
									image1.src = "http:" + obj[i].day.condition.icon; 
									image1.onload = function () {
									$('#weatherImage-1').empty().append(image1);
									};
									break;
								  case 2:
									image2.src = "http:" + obj[i].day.condition.icon; 
									image2.onload = function () {
									$('#weatherImage-2').empty().append(image2);
									};
									break;
									case 3:
									image3.src = "http:" + obj[i].day.condition.icon; 
									image3.onload = function () {
									$('#weatherImage-3').empty().append(image3);
									};
									break;
									case 4:
									image4.src = "http:" + obj[i].day.condition.icon; 
									image4.onload = function () {
									$('#weatherImage-4').empty().append(image4);
									};
									break;
									case 5:
									image5.src = "http:" + obj[i].day.condition.icon; 
									image5.onload = function () {
									$('#weatherImage-5').empty().append(image5);
									};
									break;
									case 6:
									image6.src = "http:" + obj[i].day.condition.icon; 
									image6.onload = function () {
									$('#weatherImage-6').empty().append(image6);
									};
									break;
									case 7:
									image7.src = "http:" + obj[i].day.condition.icon; 
									image7.onload = function () {
									$('#weatherImage-7').empty().append(image7);
									};
									break;
								  default:
									text = "Error. Index does not match a case";
								} 
								
								
							
						
						}
					
				}
			});
		}
		
		
		
		
		
		</script>
		
		
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDX_pImCjOZn_IBH8lK7AHFpf3_UIZNVNA&callback=initMap">
		</script>
	
	
	
	</div>
	
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
</body>

</html>