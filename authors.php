

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
	<?php include("partial/_meta"); ?>
	<?php include("partial/_scripts.php"); ?>
  <title>Authors page</title>
  <script type="text/javascript" src="https://cdn.lacompagniefrancaise.com/xjs/src/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">



</head>
<style>
/* Three columns side by side */
.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

/* Display the columns below each other instead of side by side on small screens */
@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

/* Add some shadows to create a card effect */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

/* Some left and right padding inside the container */
.container {
  padding: 0 16px;
}

/* Clear floats */
.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
</style>
<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">
	
	
	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">
	<header class="bg-primary text-center py-5 mb-4">
  <div class="container">
    <h1 class="font-weight-light text-white">InfoQuake+</h1>
	<h3 class="font-weight-light text-white">Meet the Team</h2>
  </div>
</header>

<!-- team stuff to edit -->
<div class="container">
  <div class="row">
    <!-- Milhouse -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
	  <!--- pic size 500x350 --->
        <img src="img/Ryan.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Ryan Cowan</h5>
          <div class="card-text text-black-40">Intergration Tester</div>
		  <div class="card-text text-black-40">Scrum Master</div>
		 <!--- <div class="card-text text-black-50">bio/talk about our pages?</div> -->
		  <br><a href="mailto:rcowan201@caledonian.ac.uk" target="_blank" class="btn btn-primary">Contact</a>
		  
        </div>
      </div>
    </div>
    <!-- PAPA PAUL -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
	  <!--- pic size 500x350 --->
        <img src="img/Papa.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Paul Kerr </h5>
			<div class="card-text text-black-40">Web Developer</div>          
		  <div class="card-text text-black-40">UI Designer</div>		  
		  <br><a href="mailto:pkerr205@caledonian.ac.uk" target="_blank" class="btn btn-primary">Contact</a>
        </div>
      </div>
    </div>
    <!-- SCOTTY -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
	  <!--- pic size 500x350 --->
        <img src="img/scott.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
         <h5 class="card-title mb-0">Scott Robertson</h5>
         <div class="card-text text-black-40">Web Developer</div>
		  <div class="card-text text-black-40">Unit Tester</div>
		  <br><a href="mailto:srober233@caledonian.ac.uk" target="_blank" class="btn btn-primary">Contact</a>
        </div>
      </div>
    </div>
    <!-- KIRSTEN -->
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
	  <!--- pic size 500x350 --->
        <img src="img/Kirsten.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Kirsten Paterson</h5>
          <div class="card-text text-black-40">Acceptance Tester</div>
		  <div class="card-text text-black-40">UX Designer</div>
		  
		  <br><a href="mailto:kpater207@caledonian.ac.uk" target="_blank" class="btn btn-primary">Contact</a>
        </div>
      </div>
    </div>
	</div>
	<div class="row align-items-center justify-content-center"> 
	 <!-- TODOR -->
	<div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
	  <!--- pic size 500x375 --->
        <img src="img/Todor.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Todor Radic</h5>
			<div class="card-text text-black-40">Web Developer</div>
		  <div class="card-text text-black-40">Systems Tester</div>
		  <br><a href="mailto:tradic200@caledonian.ac.uk" target="_blank" class="btn btn-primary">Contact</a>
        </div>
      </div>
    </div>
	<!-- JASMINE -->
	<div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-0 shadow">
	  <!--- pic size 500x375 --->
        <img src="img/Jasmine.jpg" class="card-img-top" alt="...">
        <div class="card-body text-center">
          <h5 class="card-title mb-0">Jasmine Baird</h5>
          <div class="card-text text-black-40">Product Owner</div>
		  <div class="card-text text-black-40">Usability Tester</div>
		  <br><a href="mailto:jbaird203@caledonian.ac.uk" target="_blank" class="btn btn-primary">Contact</a>
        </div>
      </div>
    </div>
  </div>
  </div>

</div>

		
	</div>
	
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>

	
</body>

</html>