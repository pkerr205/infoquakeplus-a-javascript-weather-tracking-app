<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Earthquake Page</title>

<?php include("partial/_meta"); ?>

<?php include("partial/_scripts.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/markerclustererplus/2.1.4/markerclusterer.js"></script>
<style>
  /* Always set the map height explicitly to define the size of the div
   * element that contains the map. */
  #map {
	height: 650px;
	max-width:850px;
  /* Optional: Makes the sample page fill the window. */
  html, body {
	height: 100%;
	margin: 0;
	padding: 0;
  }
</style>

</head>

<body>

	<div class="d-flex" id="wrapper">
	
	<?php include("partial/_sidebar"); ?>	
	

	<div id="page-content-wrapper">
	
	
	<?php include("partial/_header.php"); ?>
	
	<div class="container-fluid">

	<h1>Earthquakes</h1>
	
	<p>All earthquakes in the last week. Click on an icon for details.</p>
	
	<div class="row">
	<div class="col-sm-3">
	<div id="btn-feed" class="btn-group-vertical">
		<button type="button" class="btn btn-primary" id="significant"> Significant Earthquakes</button>
		<button type="button" class="btn btn-primary active" id="4.5"> M4.5+ Earthquakes</button>
		<button type="button" class="btn btn-primary" id="2.5"> M2.5+ Earthquakes</button>
		<button type="button" class="btn btn-primary" id="1.0"> M1.0+ Earthquakes</button>
		<button type="button" class="btn btn-primary" id="all"> All Earthquakes</button>
		<div>&nbsp;</div>
	</div>
	</div>
	<div class="col-sm-3">
	<div id="btn-time" class="btn-group-vertical">
		<button type="button" class="btn btn-info" id="hour"> Past Hour</button>
		<button type="button" class="btn btn-info active" id="day"> Past Day</button>
		<button type="button" class="btn btn-info" id="week"> Past 7 Days</button>
		<button type="button" class="btn btn-info" id="month"> Past 30 Days</button>
	</div>
	</div>
	</div>
	<p>&nbsp;</p>
	<div id="map"></div>
    <script>
        var map;
        //initMap() called when Google Maps API code is loaded - when web page is opened/refreshed 
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 2,
                center: new google.maps.LatLng(2.8, -187.3), // Center Map. Set this to any location that you like
                mapTypeId: 'terrain' // can be any valid type				
            });
		
			$.ajax({
				// The URL of the specific data required
				url: "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_day.geojson",

				// The data has succesfully loaded
				success: function (data) {
					var markers = [];
					$.each(data.features, function (key, val) {
						// Get the lat and lng data for use in the markers
						var coords = val.geometry.coordinates;
						var latLng = new google.maps.LatLng(coords[1], coords[0]);
						// Now create a new marker on the map
						var marker = new google.maps.Marker({
							position: latLng,
							map: map,
							label: val.properties.mag.toString() // Whatever label you like. This one is the magnitude of the earthquake
						});
						markers.push(marker);
						// Form a string that holds desired marker infoWindow content. The infoWindow will pop up when you click on a marker on the map
						var infowindow = new google.maps.InfoWindow({
							content: "<h3>" + val.properties.title + "</h3><p><a target='_blank' href='geodata.php?name=" + val.properties.title + "&data1=" + coords[1] + "&data2=" + coords[0] + "'>Details</a></p>"
						});
						marker.addListener('click', function (data) {
							infowindow.open(map, marker); // Open the Google maps marker infoWindow
						});
					});
					var markerCluster = new MarkerClusterer(map, markers);

				}
			});
        }

        var thelocation;
        var titleName;
		var selectedFeed;
		var selectedTime;
        $(document).ready(function () {
			
			$("#btn-feed > .btn").click(function(){
				$("#btn-feed > .btn").removeClass("active");
				$(this).addClass("active");
			});
			
			$("#btn-time > .btn").click(function(){
				$("#btn-time > .btn").removeClass("active");
				$(this).addClass("active");
			});
			
            $('.btn-group-vertical').on('click', function() {
                // Set Google map  to its start state
                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 2,
                    center: new google.maps.LatLng(2.8, -187.3), // Center Map. Set this to any location that you like
                    mapTypeId: 'terrain' // can be any valid type
                }); 
				selectedFeed = ($('#btn-feed .active').attr('id'));
				selectedTime = ($('#btn-time .active').attr('id'));
				console.log(selectedFeed + "_" +selectedTime);
                $.ajax({

						url: "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/" + selectedFeed + "_" +selectedTime + ".geojson",
					

                    // Called if there is a problem loading the data
                    error: function () {
                        $('#info').html('<p>An error has occurred</p>');
                    },

                    // The data has succesfully loaded
                    success: function (data) {
						var markers = [];
                        $.each(data.features, function (key, val) {
                            // Get the lat and lng data for use in the markers
                            var coords = val.geometry.coordinates;
                            var latLng = new google.maps.LatLng(coords[1], coords[0]);
                            // Now create a new marker on the map
                            var marker = new google.maps.Marker({
                                position: latLng,
                                map: map,
                                label: val.properties.mag.toString() // Whatever label you like. This one is the magnitude of the earthquake
                            });
							markers.push(marker);
                            // Form a string that holds desired marker infoWindow content. The infoWindow will pop up when you click on a marker on the map
							var infowindow = new google.maps.InfoWindow({
                                content: "<h3>" + val.properties.title + "</h3><p><a target='_blank' href='geodata.php?name=" + val.properties.title + "&data1=" + coords[1] + "&data2=" + coords[0] + "'>Details</a></p>"
                            });
                            marker.addListener('click', function (data) {
                                infowindow.open(map, marker); // Open the Google maps marker infoWindow
                            });
                        });
						var markerCluster = new MarkerClusterer(map, markers);

                    }
                });
            });
        });
		

    </script>
	
	
	
<!-- 	<div id="map"></div>
	
	<script>
	var map;
	  
	function initMap() 
	{
		map = new google.maps.Map(document.getElementById('map'), 
		{
			center: {lat: 10.640739, lng: 122.968956},
			zoom: 4
		});
		
		
		map.data.loadGeoJson('https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson');
		
		
		map.data.loadGeoJson('https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_month.geojson', null, function (features) {

			var markers = features.map(function (feature) {
				var g = feature.getGeometry();
				var marker = new google.maps.Marker({ 
					'position': g.get(0),
				});
				return marker;
			});
			var markerCluster = new MarkerClusterer(map, markers,{ imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' });
		});
		
		
		var infowindow = new google.maps.InfoWindow();
		


		// When the user clicks, open an infowindow
		map.data.addListener('click', function(event) {
			var myHTML = "<p><b>Area</b>: " + event.feature.getProperty("place") + 
			"</p><p><b>Magnitude</b>: " + event.feature.getProperty("mag");
			infowindow.setContent("<div style='width:150px;'>"+myHTML+"</div>");
			// position the infowindow on the marker
			infowindow.setPosition(event.feature.getGeometry().get());
			// anchor the infowindow on the marker
			infowindow.setOptions({pixelOffset: new google.maps.Size(0,-30)});
			infowindow.open(map);
		});
		


		// Use custom marker from "icon" field in the geojson
		map.data.setStyle(function(feature) {
          var magnitude = feature.getProperty('mag');
          return {
            icon: getCircle(magnitude)
          };
		});
		
		
	}
	

	
	function getCircle(magnitude) 
	{
		return {
		  path: google.maps.SymbolPath.CIRCLE,
		  fillColor: 'red',
		  fillOpacity: .2,
		  scale: Math.pow(2, magnitude) / 2,
		  strokeColor: 'white',
		  strokeWeight: .5
		};
	}

	/*function eqfeed_callback(results) 
	{
		map.data.addGeoJson(results);
	}*/	 
  
    </script> -->
	
	
	
		
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDX_pImCjOZn_IBH8lK7AHFpf3_UIZNVNA&callback=initMap" async defer>
	</script>	
		
	</div>
	
	</div>
	
	<?php include("partial/_footerScripts.php"); ?>
	
	
</body>

</html>